"""
User class handles the details of a user: password, login, etc.
"""
import requests, json, pygame, time
from session import Session
from constants import *

class MentroUser:
  def __init__(self):
    self.email      = ''
    self.password   = ''
    self.logged_in  = False
    self.session    = Session(self)
    self.token      = ''
    self.threshold  = 60
    self.attention  = 0
    self.meditation = 0
    self.poorSignal = 200
    self.messages   = []

  def __str__(self):
    return self.email

  def login(self):
    payload = {'user': {'email': self.email, 'password': self.password}, "utf8": u"\u2713"}
    try:
      req = requests.post(SITE + DETAILS + '.json', data=json.dumps(payload), \
        headers=REQ_HEADERS)
      if req.status_code == 200:
        data = req.json()
        self.token = data['key']
        self.threshold = data['message']
        self.logged_in = True
        self.messages.append([time.time(), "Logged in. Press circle icon to record.", NOTICE])
      elif req.status_code == 422:
        self.logged_in = False
        data = req.json()
        self.messages.append([time.time(), "Error logging in: " + data['error'], WARN])
    except requests.exceptions.ConnectionError as ex:
      self.messages.append([time.time(), "Mentronome site not available. " +\
        "Check network settings.", WARN])
      self.logged_in = False
    pygame.display.flip()