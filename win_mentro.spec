# -*- mode: python -*-
a = Analysis(['mentro.py'],
             pathex=['C:\\Users\\ronan\\Documents\\mentronome'],
             hiddenimports=[],
             hookspath=None)
a.datas += [('images\\candle.png','C:\\Users\\ronan\\Documents\\mentronome\\images\\candle.png','DATA')]
a.datas += [('images\\logo.png','C:\\Users\\ronan\\Documents\\mentronome\\images\\logo.png','DATA')]
a.datas += [('images\\logo.ico','C:\\Users\\ronan\\Documents\\mentronome\\images\\logo.ico','DATA')]
a.datas += [('images\\mail.png','C:\\Users\\ronan\\Documents\\mentronome\\images\\mail.png','DATA')]
a.datas += [('images\\mute.png','C:\\Users\\ronan\\Documents\\mentronome\\images\\mute.png','DATA')]
a.datas += [('images\\help.png','C:\\Users\\ronan\\Documents\\mentronome\\images\\help.png','DATA')]
a.datas += [('images\\pause.png','C:\\Users\\ronan\\Documents\\mentronome\\images\\pause.png','DATA')]
a.datas += [('images\\record.png','C:\\Users\\ronan\\Documents\\mentronome\\images\\record.png','DATA')]
a.datas += [('images\\unlit_candle.png','C:\\Users\\ronan\\Documents\\mentronome\\images\\unlit_candle.png','DATA')]
a.datas += [('images\\volume.png','C:\\Users\\ronan\\Documents\\mentronome\\images\\volume.png','DATA')]
a.datas += [('images\\Vera.ttf','C:\\Users\\ronan\\Documents\\mentronome\\images\\Vera.ttf','DATA')]
a.datas += [('audio\\candle.wav','C:\\Users\\ronan\\Documents\\mentronome\\audio\\candle.wav','DATA')]
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=1,
          name=os.path.join('build\\pyi.win32\\mentro', 'mentronome.exe'),
          debug=False,
          strip=None,
          upx=True,
	  icon=os.path.join('images', 'logo.ico'),
          console=False, version='win_version.txt' )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name=os.path.join('dist', 'mentro'))
app = BUNDLE(coll,
             name=os.path.join('dist', 'mentro'))
