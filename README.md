Mentronome Desktop
==================

Meditation measurement application. Talks to Neurosky Mindwave over socket and saves data to [Mentronome site](http://www.mentronome.com). 
See http://www.mentronome.com/help for further details. 

Released under the [Mozilla Public License 2.0 (MPL-2.0)](http://opensource.org/licenses/MPL-2.0)

To run on your platform
-----------------------

 * Install Python 2.7 - http://www.python.org/download/releases/
 * Install Pygame - http://www.pygame.org/install.html
 * Ensure you have Neurosky's  'ThinkGear Connector' installed
 * Run mentro.py

Detailed Installation on Mac OS 10.8 (Mountain Lion)
----------------------------------------------------

 1. Perform following instructions from the command line via a Mac Terminal. Indicated with > or different font
 2. Install Homebrew for Mac. Follow instructions at http://mxcl.github.io/homebrew/
 3. > brew install python
 4. > /usr/local/bin/pip install numpy
 5. > /usr/local/bin/pip install requests
 6. > brew install sdl
 7. > brew install sdl_gfx sdl_image sdl_mixer sdl_ttf portmidi
 8. > /usr/local/bin/pip install hg+http://bitbucket.org/pygame/pygame
 9. Download the source code from https://bitbucket.org/RonanOD/mentronome_desktop/
 10. Unzip locally and open a new terminal window.
 11. cd (change directory) to the unzipped folder.
 12. python mentro.py
 13. If prompted to, go to http://xquartz.macosforge.org/ and install XQuartz
 14. Log back in and repeat step 12

License thanks
--------------

 * Glyphicons: http://www.glyphicons.com
 * Audio: http://www.freesound.org/people/Batuhan/sounds/17733/
 * Font: http://ftp.gnome.org/pub/GNOME/sources/ttf-bitstream-vera/1.10/
 * Pygame: http://pygame.org
 * Pyinstaller: http://www.pyinstaller.org/
 * Py2app: https://pypi.python.org/pypi/py2app/

Version History
----------------

 * 0.0.3 : Added Help button. Fixed start date to local time. Expanded calm switchover to 4 seconds.
 * 0.0.2 : Mac client support.
 * 0.0.1 : First alpha version. 