"""
 Mentronome python client. Measures meditation session for mentronome.com

 Released under the Mozilla Public License 2.0 (MPL-2.0)
 See http://opensource.org/licenses/MPL-2.0
"""
import pygame, time, sys, asyncore, webbrowser
from pygame.locals import *
from mentro_user import MentroUser
from sprite_strip_anim import SpriteStripAnim
from mindwave_client import MindwaveClient
from constants import *

# Application variables
emailFocus = True
buttonBack = WHITE
unmuted    = True

def main():
  init()
  while True: # main game loop
    drawAll()
    checkForQuit()
    for event in pygame.event.get():
      if event.type == QUIT:
        terminate()
      elif event.type == KEYDOWN:
        handleKeyPress(event)
      elif event.type == MOUSEBUTTONUP:
        handleMouseUp(event)
      elif event.type == MOUSEBUTTONDOWN:
        handleMouseDown(event)
      elif event.type == USEREVENT:
        USER.session.tick()
    asyncore.poll(timeout=.01)
    pygame.display.update()
    FPSCLOCK.tick(FPS)

def init():
  global FONT, DISPLAYSURF, FPSCLOCK, CLIENT, USER, MAIL, RECORD, PAUSE, \
         STRIP, AUDIO, MUTE, UNMUTE, HELP
  print "Mentronome Client v.", VERSION
  print "www.mentronome.com"
  pygame.init()
  pygame.display.set_icon(ICON)
  DISPLAYSURF = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
  pygame.display.set_caption('Mentronome')
  FONT = pygame.font.Font(os.path.join(BASEDIR, os.path.normpath('images/Vera.ttf')), 14)
  MAIL = MAIL.convert_alpha()
  RECORD = RECORD.convert_alpha()
  PAUSE = PAUSE.convert_alpha()
  MUTE = MUTE.convert_alpha()
  UNMUTE = UNMUTE.convert_alpha()
  HELP = HELP.convert_alpha()
  STRIP = SpriteStripAnim(os.path.join(BASEDIR, os.path.normpath('images/candle.png')), \
          STRIP_RECT, NUM_FRAMES, 1, True, FRAMES)
  pygame.mixer.init()
  AUDIO = pygame.mixer.Sound(os.path.join(BASEDIR, os.path.normpath('audio/candle.wav')))
  AUDIO.set_volume(VOLUME)
  status = DISCONNECTED
  # Setup the user TODO: Cache email so don't have to re-enter each time.
  USER = MentroUser()
  CLIENT = MindwaveClient(USER)
  # Clock for synchronizing UI.
  FPSCLOCK = pygame.time.Clock()
  pygame.time.set_timer(USEREVENT, 1000)

def handleKeyPress(event):
  global emailFocus
  key = event.key
  if not(USER.logged_in):
    if key == K_BACKSPACE:
      if emailFocus:
        if len(USER.email) > 0:
          USER.email = USER.email[0:-1]
      else:
        if len(USER.password) > 0:
          USER.password = USER.password[0:-1]
    elif key == K_RETURN:
      USER.login()
    elif key == K_TAB:
      emailFocus = not(emailFocus)
    elif key <= 127:
      if emailFocus:
        USER.email += event.unicode
      else:
        USER.password += event.unicode
    pygame.display.flip()

def handleMouseUp(event):
  global emailFocus, buttonBack, unmuted
  x, y = event.pos
  if HELP_RECT.collidepoint( (x, y)):
    x = webbrowser.open_new(HELP_SITE)
    print "X", x
  if not(USER.logged_in):
    if EMAIL.collidepoint( (x, y) ):
      emailFocus = True
    elif PASSWORD.collidepoint( (x, y) ):
      emailFocus = False
    if LOGIN.collidepoint( (x, y) ):
      buttonBack = WHITE
      USER.login()
  else:
    if RECORD_RECT.collidepoint( (x, y) ):
      USER.session.toggle()
    if MAIL_RECT.collidepoint( (x, y) ):
      USER.session.send()
    if MUTE_RECT.collidepoint( (x, y) ):
      unmuted = not(unmuted)
      if unmuted:
        AUDIO.set_volume(VOLUME)
      else:
        AUDIO.set_volume(0)
  pygame.display.flip()

def handleMouseDown(event):
  global buttonBack
  x, y = event.pos
  if not(USER.logged_in):
    if LOGIN.collidepoint( (x, y) ):
      buttonBack = GRAY
    pygame.display.flip()

def drawAll():
  DISPLAYSURF.fill(BLACK) # Background
  drawCandle()
  drawBars()
  if USER.logged_in:
    drawControls()
  else:
    drawLogin()
  drawDisplayText()
  drawProgressBar()
  drawMessages()

def drawCandle():
  #STRIP.iter()
  if USER.session.calm:
    #TODO: Have candle slowly sink over time.
    AUDIO.play(-1, 0, FADEOUT_MS)
    image = STRIP.next()
    DISPLAYSURF.blit(image, (0,0))
    FPSCLOCK.tick(FPS)
  else:
    AUDIO.fadeout(FADEOUT_MS)
    DISPLAYSURF.blit(CANDLE, (0,0))

def drawBars():
  global HELP_RECT
  HELP_RECT = DISPLAYSURF.blit(HELP, HELP_X_Y)
  if USER.poorSignal == 200:
    pygame.draw.rect(DISPLAYSURF, RED, BAR_1)
  elif USER.poorSignal == 0:
    pygame.draw.rect(DISPLAYSURF, GREEN, BAR_1)
    pygame.draw.rect(DISPLAYSURF, GREEN, BAR_2)
    pygame.draw.rect(DISPLAYSURF, GREEN, BAR_3)
  else:
    pygame.draw.rect(DISPLAYSURF, GRAY, BAR_1)
    pygame.draw.rect(DISPLAYSURF, GRAY, BAR_2)

def drawLogin():
  pygame.draw.rect(DISPLAYSURF, WHITE, EMAIL, 1)
  pygame.draw.rect(DISPLAYSURF, WHITE, PASSWORD, 1)
  pygame.draw.rect(DISPLAYSURF, buttonBack, LOGIN)
  emailTextSurf = FONT.render(EMAIL_LABEL_TEXT + USER.email, True, WHITE, BLACK)
  DISPLAYSURF.blit(emailTextSurf, EMAIL_LABEL)
  mask = '*' * len(USER.password)
  passTextSurf = FONT.render(PASSWORD_LABEL_TEXT + mask, True, WHITE, BLACK)
  DISPLAYSURF.blit(passTextSurf, PASSWORD_LABEL)
  loginTextSurf = FONT.render(LOGIN_BUTTON_TEXT, True, BLACK, buttonBack)
  DISPLAYSURF.blit(loginTextSurf, LOGIN_LABEL)
  # Cursor. Draws every even second.
  if emailFocus:
    top = (EMAIL.left + emailTextSurf.get_width() + 2, EMAIL.top + 3)
    bot = (top[0], top[1] + emailTextSurf.get_height() - 1)
  else:
    top = (PASSWORD.left + passTextSurf.get_width() + 2, PASSWORD.top + 3)
    bot = (top[0], top[1] + passTextSurf.get_height() - 1)
  secs = int(time.time())
  if secs % 2 == 0:
    pygame.draw.line(DISPLAYSURF, WHITE, top, bot)

def drawControls():
  global MAIL_RECT, RECORD_RECT, MUTE_RECT
  if USER.session.recording:
    RECORD_RECT = DISPLAYSURF.blit(PAUSE, PAUSE_X_Y)
  else:
    RECORD_RECT = DISPLAYSURF.blit(RECORD, RECORD_X_Y)
  MAIL_RECT = DISPLAYSURF.blit(MAIL, MAIL_X_Y)
  if unmuted:
    MUTE_RECT = DISPLAYSURF.blit(MUTE, MUTE_X_Y)
  else:
    MUTE_RECT = DISPLAYSURF.blit(UNMUTE, UNMUTE_X_Y)

def drawDisplayText():
  if USER.poorSignal == 0:
    status = "Att: " + str(USER.attention) + " Med: " + str(USER.meditation)
  else:
    status = DISCONNECTED
  textSurfaceObj = FONT.render(status, True, WHITE, BLACK)
  textRectObj = textSurfaceObj.get_rect()
  textRectObj.center = (int(WINDOW_WIDTH / 2), int(WINDOW_HEIGHT - 100))
  DISPLAYSURF.blit(textSurfaceObj, textRectObj)

def drawProgressBar():
  if USER.meditation > 0:
    innerRect = PB.copy()
    innerRect.width = USER.meditation * 5
    pygame.draw.rect(DISPLAYSURF, BLUE, innerRect)
  pygame.draw.rect(DISPLAYSURF, WHITE, PB, 1)
  apogee = USER.threshold * 5
  # 1   3
  #   2
  point_one   = [PB_LEFT + apogee - TRI_HALF, PB_TOP - TRIANGLE_HEIGHT]
  point_two   = [point_one[0] + TRI_HALF, PB_TOP]
  point_three = [point_one[0] + TRIANGLE_HEIGHT, point_one[1]]
  thresholdCoords = [ point_one, point_two, point_three ]
  pygame.draw.polygon(DISPLAYSURF, BLUE, thresholdCoords)
  FPSCLOCK.tick(FPS)

def drawMessages():
  if len(USER.messages) > 0:
    idx = len(USER.messages) - 1
    item  = USER.messages[idx]
    start, text, state = item
    now = time.time()
    if now > start + MSG_DURATION:
      del USER.messages[idx]
      pygame.display.flip()
    else:
      if state == WARN:
        fg     = DARK_YELLOW
        bg     = BKG_YELLOW
        border = BRDR_YELLOW
      elif state == NOTICE:
        fg = DARK_GREEN
        bg = BKG_GREEN
        border = BRDR_GREEN
      textSurfaceObj = FONT.render(text, True, fg, bg)
      textRectObj = textSurfaceObj.get_rect()
      textRectObj.center = (int(WINDOW_WIDTH / 2), MESSAGE_TOP)
      textBg = textRectObj.copy()
      textBg.width += 10
      textBg.height += 10
      textBg.x -= 5
      textBg.y -= 5
      pygame.draw.rect(DISPLAYSURF, bg, textBg)
      pygame.draw.rect(DISPLAYSURF, border, textBg, 1)
      DISPLAYSURF.blit(textSurfaceObj, textRectObj)

def terminate():
  CLIENT.close_socket()
  pygame.quit()
  sys.exit()

def checkForQuit():
    for event in pygame.event.get(QUIT): # get all the QUIT events
        terminate() # terminate if any QUIT events are present
    for event in pygame.event.get(KEYUP): # get all the KEYUP events
        if event.key == K_ESCAPE:
            terminate() # terminate if the KEYUP event was for the Esc key
        pygame.event.post(event) # put the other KEYUP event objects back

if __name__ == '__main__':
  main()
