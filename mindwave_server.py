"""
Simple test socket server so Mindwave handset doesn't have
to run all the time, for testing, etc. Mindwave connector needs to
be stopped before this class can run. Currently not multi-threaded.
TODO: Add different eegPowers
"""
import SocketServer, json, random, time, os, sys

HOST       = "127.0.0.1"
PORT       = 13854
PAUSE      = 0.1
TERMINATOR = "\r"

class MindwaveServer(SocketServer.BaseRequestHandler):
  def handle(self):
    try:
      # self.request is the TCP socket connected to the client
      received = self.request.recv(1024)
      print "REC", received
      self.data = json.loads(received)
      print "DATA", self.data
      if 'enableRawOutput' in self.data.keys() \
            and 'format' in self.data.keys() \
            and self.data['enableRawOutput'] == False \
            and self.data['format'] == 'Json':
          # Give some poor signal before starting proper
          resp = {'poorSignalLevel': 200}
          print "RESPONDING"
          for count in range(0, 30):
            result = self.request.send(json.dumps(resp) + TERMINATOR)
            time.sleep(PAUSE)
          # Then 5 lines at signal level of 125. But starting to give data
          resp['poorSignalLevel'] = 125
          resp['eSense'] = {"attention": 0, "meditation": 0}
          resp['eegPower'] = {"delta":     0,
                              "theta":     0,
                              "lowAlpha":  0,
                              "highAlpha": 0,
                              "lowBeta":   0,
                              "highBeta":  0,
                              "lowGamma":  0,
                              "highGamma": 0}
          for count in range(0, 5):
            self.request.send(json.dumps(resp) + TERMINATOR)
            time.sleep(PAUSE)
          # Then 5 more at about 50%
          resp['poorSignalLevel'] = 54
          for count in range(0, 5):
            self.request.send(json.dumps(resp) + TERMINATOR)
            time.sleep(PAUSE)
          # Then a few at full signal but still no meditation/attention
          resp['poorSignalLevel'] = 0
          for count in range(0, 10):
            self.request.send(json.dumps(resp) + TERMINATOR)
            time.sleep(PAUSE)
          while True: # Main response loop.
            # Send an eyeblink every 50 or so.
            if random.randrange(0, 101) == 50:
              self.request.send(json.dumps({"blinkStrength": 10}) + TERMINATOR)
            else:
              resp['eSense']['attention']  = random.randrange(0, 101)
              resp['eSense']['meditation'] = random.randrange(0, 101)
              self.request.send(json.dumps(resp) + TERMINATOR)
            time.sleep(PAUSE)
      else:
        raise "Opening communication now valid. Must be {'enableRawOutput': false, 'format': 'Json'}"
    except Exception as e:
      exc_type, exc_obj, exc_tb = sys.exc_info()
      print "ERROR", exc_type, exc_obj

if __name__ == "__main__":
  server = SocketServer.TCPServer((HOST, PORT), MindwaveServer)
  print "Starting Test Mindwave Server on port", PORT
  # Activate the server; this will keep running until you
  # interrupt the program with Ctrl-C
  server.serve_forever()