## remove previous builds.  Start with clean slate.
rm -rf build dist

## Force python into 32 bit mode.
#export VERSIONER_PYTHON_PREFER_32_BIT=yes

## Force build with custom installed python
#/Library/Frameworks/Python.framework/Versions/2.6/bin/python setup.py py2app
#pygame,time,sys,asyncore,asynchat,socket,json,requests

python setup_py2app.py py2app #--debug-skip-macholib

#-i constants.py,mindwave_client.py,session.py,sprite_strip_anim.py,spritesheet.py,mentro_user.py --site-packages

open dist/mentro.app