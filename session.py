""" 
Class for handling a user session: recording, syncing to server, etc.
"""
import requests, json, time
from constants import *

class Session:
  def __init__(self, user):
    self.user       = user
    self.reset()

  def reset(self):
    self.duration   = 0
    self.start_time = None
    self.recording  = False
    self.in_med     = 0
    self.token      = None
    self.calm       = False
    self.fudge_time = 0

  def tick(self):
    if self.recording:
      self.duration += 1
      if self.user.meditation >= self.user.threshold:
        self.in_med += 1
        self.calm = True
        self.fudge_time = 0
      else:
        if self.fudge_time > 3:
          self.calm = False
        else:
          self.fudge_time += 1

  def toggle(self):
    self.recording = not(self.recording)
    if self.recording:
      self.record()
    else:
      self.pause()

  def record(self):
    self.recording = True
    self.user.messages.append([time.time(), "Recording. Press pause or send to finish.", NOTICE])
    if self.duration == 0:
      self.start_time = time.localtime()

  def pause(self):
    self.user.messages.append([time.time(), "Paused. Press record or send to finish.", NOTICE])

  def percent(self):
    answer = 0
    if self.duration > 0 and self.in_med > 0:
      answer = int(100 * float(self.in_med) / float(self.duration))
    return answer

  def iso_start_time(self):
    return time.strftime('%Y-%m-%d %H:%M:%S', self.start_time)

  def send(self):
    if not(self.recording):
      self.user.messages.append([time.time(), "Must press record before sending session.", WARN])
      return
    self.recording = False
    payload = {'d': self.duration, 't': self.user.token, 'e': self.user.email, \
               'p': self.percent(), 's': self.iso_start_time()}
    req = requests.post(SITE + SESSION_CREATE + '.json', \
              data=json.dumps(payload), headers=REQ_HEADERS)
    data = req.json()
    if req.status_code == 200 \
      and not(data == None) and data['message'] and data['message'] == 'OK':
        self.user.messages.append([time.time(), "Session saved. Press Escape key to exit.", NOTICE])
        self.reset()
    else:
      data = req.json()
      self.user.messages.append([time.time(), "Error saving session: " + data['error'], WARN])