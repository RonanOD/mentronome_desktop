"""
Constants for use by mentro application.
"""
import pygame, os, sys

# Graphics
WINDOW_WIDTH    = 640
WINDOW_HEIGHT   = 480
PB_LEFT         = 70
PB_TOP          = WINDOW_HEIGHT - 60
PB_WIDTH        = WINDOW_WIDTH - 140
PB_HEIGHT       = 20
TEXT_TOP        = 120
TEXT_LEFT       = 130
TEXT_HEIGHT     = 24
TEXT_WIDTH      = WINDOW_WIDTH - 260
BUTTON_WIDTH    = 60
TRIANGLE_HEIGHT = 10
TRI_HALF        = TRIANGLE_HEIGHT / 2
MESSAGE_TOP     = 28
RECORD_X_Y      = (12, 12)
MAIL_X_Y        = (12, 44)
PAUSE_X_Y       = (20, 16)
MUTE_X_Y        = (14, 80)
UNMUTE_X_Y      = (14, 75)
HELP_X_Y        = (WINDOW_WIDTH - 68, 12)
BAR_LINE        = 36
STRIP_RECT      = (0,0,640,480)
NUM_FRAMES      = 16

# Time
FPS          = 30
MSG_DURATION = 4
FRAMES       = 1
FADEOUT_MS   = 200
VOLUME       = 0.5

# Strings
SITE                = "http://mentronome.com/"
HELP_SITE           = SITE + "help"
DETAILS             = "treshold/details"
SESSION_CREATE      = "sittings/token_create"
DISCONNECTED        = "Disconnected"
EMAIL_LABEL_TEXT    = "Email: "
PASSWORD_LABEL_TEXT = "Password: "
LOGIN_BUTTON_TEXT   = "Login"
WARN                = "Alert"
NOTICE              = "Notice"
VERSION             = "0.0.3"

# Colours
BLACK         = (0,   0,   0)
GRAY          = (190, 190, 190)
WHITE         = (255, 255, 255)
RED           = (255, 0,   0)
GREEN         = (0,   255, 0)
BKG_GREEN     = (223, 240, 216)
BRDR_GREEN    = (223, 240, 216)
DARK_GREEN    = (70,  136, 71)
BLUE          = (0,   0,   255)
BKG_YELLOW    = (252, 248, 227)
BRDR_YELLOW   = (214, 233, 198)
DARK_YELLOW   = (192, 152, 83)

# UI Components
BASEDIR       = None
if getattr(sys, 'frozen', None) and hasattr(sys, '_MEIPASS'):
  BASEDIR = sys._MEIPASS
else:
  BASEDIR = os.path.dirname(__file__)
  if BASEDIR.endswith('.zip'):
    # Hack for Mac OS app
    lis = BASEDIR.split('Contents/Resources/')
    BASEDIR = lis[0] + 'Contents/Resources/'

MAIL           = pygame.image.load(os.path.join(BASEDIR, os.path.normpath('images/mail.png')))
RECORD         = pygame.image.load(os.path.join(BASEDIR, os.path.normpath('images/record.png')))
PAUSE          = pygame.image.load(os.path.join(BASEDIR, os.path.normpath('images/pause.png')))
MUTE           = pygame.image.load(os.path.join(BASEDIR, os.path.normpath('images/mute.png')))
UNMUTE         = pygame.image.load(os.path.join(BASEDIR, os.path.normpath('images/volume.png')))
CANDLE         = pygame.image.load(os.path.join(BASEDIR, os.path.normpath('images/unlit_candle.png')))
ICON           = pygame.image.load(os.path.join(BASEDIR, os.path.normpath('images/logo.png')))
HELP           = pygame.image.load(os.path.join(BASEDIR, os.path.normpath('images/help.png')))
RECORD_RECT    = pygame.Rect(0,0,0,0)
MAIL_RECT      = RECORD_RECT
HELP_RECT      = RECORD_RECT
BAR_1          = pygame.Rect(WINDOW_WIDTH - 36, BAR_LINE, 6, -6)
BAR_2          = pygame.Rect(WINDOW_WIDTH - 28, BAR_LINE, 6, -12)
BAR_3          = pygame.Rect(WINDOW_WIDTH - 20, BAR_LINE, 6, -18)
EMAIL          = pygame.Rect(TEXT_LEFT, TEXT_TOP, TEXT_WIDTH, TEXT_HEIGHT)
PASSWORD       = pygame.Rect(TEXT_LEFT, TEXT_TOP + TEXT_HEIGHT * 2, TEXT_WIDTH, TEXT_HEIGHT)
LOGIN          = pygame.Rect(TEXT_LEFT + TEXT_WIDTH - BUTTON_WIDTH, TEXT_TOP + (TEXT_HEIGHT * 4), \
                             BUTTON_WIDTH, TEXT_HEIGHT)
EMAIL_LABEL    = pygame.Rect(TEXT_LEFT + 2, TEXT_TOP + 5, TEXT_WIDTH, TEXT_HEIGHT)
PASSWORD_LABEL = pygame.Rect(TEXT_LEFT + 2, TEXT_TOP + (TEXT_HEIGHT * 2) + 5, TEXT_WIDTH, TEXT_HEIGHT)
LOGIN_LABEL    = pygame.Rect(TEXT_LEFT + TEXT_WIDTH - BUTTON_WIDTH + 12, TEXT_TOP + (TEXT_HEIGHT * 4) + 5, \
                             BUTTON_WIDTH, TEXT_HEIGHT)
PB             = pygame.Rect(PB_LEFT, PB_TOP, PB_WIDTH, PB_HEIGHT)

# Configuration
REQ_HEADERS    = {'content-type': 'application/json', 'X-Transaction': 'POST'}
