"""
Mindwave client for connecting to mindwave headset over a socket.
"""

import asyncore, asynchat, socket, json, sys, time

SOCKET_CONFIG  = {'enableRawOutput': False, 'format': "Json"}
HOST           = "127.0.0.1"
PORT           = 13854

class MindwaveClient(asynchat.async_chat):
  def __init__(self, user):
    self.user = user
    #self.is_writable = True
    # Socket setup. See http://effbot.org/librarybook/asyncore.htm
    # Also: http://www.mail-archive.com/pygame-users@seul.org/msg07364.html
    asynchat.async_chat.__init__(self)
    self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
    self.buffer  = ""
    self.set_terminator("\r")
    self.address = (HOST, int(PORT))
    self.connect_with_config()

  # Connect and send configuration
  def connect_with_config(self):
    try:
      # TODO: give appName and appKey to connect. See tgsp guide.
      self.connect(self.address)
      self.push(json.dumps(SOCKET_CONFIG) + "\r")
    except Exception as e:
      self.user.messages.append([time.time(), "Check headset. Unable to connect.", WARN])

  def handle_connect(self):
    #print "Connected to headset"
    # connection succeeded
    pass

  def handle_expt(self):
    print "Error connecting"
    self.close() # connection failed, shutdown

  def collect_incoming_data(self, data):
    self.buffer = self.buffer + data
               
  def handle_close(self):
      self.close()

  def close_socket(self):
    self.close()

  def found_terminator(self):
    data = self.buffer
    #print "DATA", data
    self.buffer = ""
    if len(data) == 0:
      return 
    try:
      data_hash = json.loads(data)
      if 'poorSignalLevel' in data_hash:
        self.user.poorSignal = data_hash['poorSignalLevel']
        if self.user.poorSignal == 0:
          self.user.attention = data_hash['eSense']['attention']
          self.user.meditation = data_hash['eSense']['meditation']
        elif self.user.poorSignal == 200:
          self.user.attention = 0
          self.user.meditation = 0
      elif 'blinkStrength' in data_hash:
        # They blinked. Ignore for now.
        pass
      else:
        print "No Data", data_hash
    except Exception as e:
      exc_type, exc_obj, exc_tb = sys.exc_info()
      # Unparsable string from buffer.
      print "Client Socket Error", exc_type