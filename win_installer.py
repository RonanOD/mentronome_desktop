"""
Build a windows msi in the horrific Wix XML structure.
"""
import os
from constants import *

main_xml_string = ""
img_files = ""
inc_refs = ""
inc_files = ""
img_refs = ""
root_refs = ""
wxs1 = ""
wxs1 += '<?xml version="1.0" encoding="utf-8"?>\n'
wxs1 += '<Wix xmlns="http://schemas.microsoft.com/wix/2006/wi">\n'
wxs1 += '  <Product Name="Mentronome" Id="d7bfc10b-5293-4023-b62f-47ca7db8f473" UpgradeCode="fbeeec4b-b0ac-4a45-8899-ae9495f15303" Language="1033" Codepage="1252" Version="' + VERSION + '" Manufacturer="Mentronome Ltd.">'
wxs1_5 ="""
    <Package Id="*" Keywords="Installer" Description="Mentronome Installer" Comments="Mentronome is a registered trademark of Mentronome Ltd." Manufacturer="Mentronome Ltd." InstallerVersion="100" Languages="1033" Compressed="yes" SummaryCodepage="1252" Platform="x86" />
    <Media Id="1" Cabinet="mentronome.cab" EmbedCab="yes" DiskPrompt="CD-ROM #1" />
    <Property Id="DiskPrompt" Value="Mentronome's Installation [1]" />
    <Directory Id="TARGETDIR" Name="SourceDir">
      <Directory Id="ProgramFilesFolder" Name="PFiles">
        <Directory Id="INSTALLDIR" Name="Mentronome">
          <Component Id="MainExecutable" Guid="2aa9c063-05a2-4288-a8b6-0f7d73597627">
            <File Id="MentronomeEXE" Name="mentronome.exe" DiskId="1" Source="dist\mentro\mentronome.exe" KeyPath="yes">
              <Shortcut Id="startmenuMentronome" Directory="ProgramMenuDir" Name="Mentronome" WorkingDirectory="INSTALLDIR" Icon="mentronome.exe" IconIndex="0" Advertise="yes" />
              <Shortcut Id="desktopMentronome" Directory="DesktopFolder" Name="Mentronome" WorkingDirectory="INSTALLDIR" Icon="mentronome.exe" IconIndex="0" Advertise="yes" />
            </File>
          </Component>
          <!-- BEGIN ROOT DIST FILES -->
"""
main_xml_string += wxs1
main_xml_string += wxs1_5
wxs2 = """
          <!-- END ROOT DIST FILES -->
          <Directory Id="images_dir" Name="images" FileSource="dist\mentro\images"></Directory>
          <Directory Id="audio_dir" Name="audio" FileSource="dist\mentro\\audio"></Directory>
          <Directory Id="include_dir" Name="include" FileSource="dist\mentro\include"></Directory>
        </Directory>
      </Directory>
      <Directory Id="ProgramMenuFolder" Name="Programs">
        <Directory Id="ProgramMenuDir" Name="Mentronome">
          <Component Id="ProgramMenuDir" Guid="75f34b27-1db2-4ae3-991d-1648d9af1972">
            <RemoveFolder Id="ProgramMenuDir" On="uninstall" />
"""
wxs2_3 = '            <RegistryValue Root="HKCU" Key="Software\Mentronome\Version" Type="string" Value="' + VERSION + '" KeyPath="yes" />'
wxs2_7 = """
          </Component>
        </Directory>
      </Directory>
      <Directory Id="DesktopFolder" Name="Desktop" />
    </Directory>

    <DirectoryRef Id="images_dir">
      <!-- BEGIN IMAGE FILES -->
"""
wxs2 += wxs2_3 + wxs2_7
wsx3 = """
      <!-- END IMAGE FILES -->
    </DirectoryRef>

    <DirectoryRef Id="audio_dir">
      <Component Id="candle.wav" Guid="*">
        <File Id="candle.wav" Source="dist\mentro\\audio\candle.wav" KeyPath="yes" />
      </Component>
    </DirectoryRef>

    <DirectoryRef Id="include_dir">
      <!-- BEGIN INCLUDE FILES -->
"""
wsx4 = """
      <!-- END INCLUDE FILES -->
    </DirectoryRef>

    <Feature Id="Complete" Level="1">
      <ComponentRef Id="MainExecutable" />
      <ComponentRef Id="ProgramMenuDir" />
      <!-- BEGIN ROOT DIST FILE REFS -->
"""
wsx5 = """
      <!-- END ROOT DIST FILE REFS -->
      <!-- BEGIN IMAGE FILE REFS -->
"""
wsx6 = """
      <!-- END IMAGE FILE REFS -->
      <!-- BEGIN INCLUDE FILE REFS -->
"""
wsx7 = """
      <!-- BEGIN INCLUDE FILE REFS -->
      <ComponentRef Id="candle.wav" />
    </Feature>
    <Icon Id="mentronome.exe" SourceFile="dist\mentro\mentronome.exe" />
  </Product>
</Wix>
"""

for path, dirs, files in os.walk("."):
  if path == '.':
    # delete the existing sludge
    if 'mentronome.wxs' in files:
      os.remove('mentronome.wxs')
      print 'deleted .wxs'
    if 'mentronome.wixobj' in files:
      os.remove('mentronome.wixobj')
      print 'deleted .wixobj'
    if 'mentronome.wixpdb' in files:
      os.remove('mentronome.wixpdb')
      print 'deleted .wixpdb'
    if 'mentronome.msi' in files:
      os.remove('mentronome.msi')
      print 'deleted .msi'
  if path == '.\dist\mentro':
    # Do root dist files
    for f in files:
      if f == 'mentronome.exe':
        continue
      main_xml_string += \
        '          <Component Id="' + f + '" Guid="*">\n' + \
        '            <File Id="' + f + '" Source="dist\mentro\\' + f + '" KeyPath="yes" />\n' + \
        '          </Component>\n'
      root_refs += '      <ComponentRef Id="' + f + '" />\n'
  if path == '.\dist\mentro\images':
    for f in files:
      if f == 'Thumbs.db':
        continue
      img_files += \
        '          <Component Id="' + f + '" Guid="*">\n' + \
        '            <File Id="' + f + '" Source="dist\mentro\images\\' + f + '" KeyPath="yes" />\n' + \
        '          </Component>\n'
      img_refs += '      <ComponentRef Id="' + f + '" />\n'
  if path == '.\dist\mentro\include':
    for f in files:
      inc_files += \
        '          <Component Id="' + f + '" Guid="*">\n' + \
        '            <File Id="' + f + '" Source="dist\mentro\include\\' + f + '" KeyPath="yes" />\n' + \
        '          </Component>\n'
      inc_refs += '      <ComponentRef Id="' + f + '" />\n'

with open("mentronome.wxs", "w") as text_file:
  text_file.write(main_xml_string)
  text_file.write(wxs2)
  text_file.write(img_files)
  text_file.write(wsx3)
  text_file.write(inc_files)
  text_file.write(wsx4)
  text_file.write(root_refs)
  text_file.write(wsx5)
  text_file.write(img_refs)
  text_file.write(wsx6)
  text_file.write(inc_refs)
  text_file.write(wsx7)

# Call the candle light stuff
os.system("candle mentronome.wxs")
os.system("light mentronome.wixobj")
